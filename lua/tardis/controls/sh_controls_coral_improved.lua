
TARDIS:AddControl({
	id = "coral2005_throttle",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")
		local demat_seq = self.interior:GetData("demat_seq")
		local poweron_seq1 = self.interior:GetData("poweron_seq1")
		local rh = self.interior:GetData("rh")
		local brake =  self.interior:GetData("brake")
		local gs = self.interior:GetData("gswitches")
		local bs = self.interior:GetData("bswitches")
		local event = self.interior:GetData("flight_event")
		local event_seq1 =self.interior:GetData("event_seq1")
		local stabilisers = self.interior:GetData("stabilisers")

		tardisdebug(full)

        local part = self.interior:GetPart("coralpogthrottle")
		if not part:GetOn() and power and not teleport and not vortex and not full then
            self.interior:SetData("demat_seq1", true)
			part:EmitSound("poogie/2005/drum_demat.wav")
		elseif not part:GetOn() and power and not teleport and not vortex and brake and bs and rh then
			part:EmitSound("poogie/2005/drum.wav")
			self:Demat()
			if not stabilisers then
			end
		elseif vortex and event and event_seq1 then
			self.interior:SetData("event_seq2", true)
		elseif not part:GetOn() and poweron_seq1 and not power then
			self:TogglePower()
		elseif part:GetOn() then
			self:Mat()
			self.interior:SetData("demat_seq1", false)
			self.interior:SetData("full_seq1", false)
			self.interior:SetData("poweron_seq1", false)
			self.interior:SetData("event_seq1", false)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_flightlever",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local demat_seq = self.interior:GetData("demat_seq1")

		local part = self.interior:GetPart("coralpogflight")
		if part:GetOn() and power and not teleport and not vortex and demat_seq then
            part:EmitSound("poogie/2005/drum_phase.wav")
			self.interior:SetData("demat_seq2", true)
        else
			self.interior:SetData("demat_seq2", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2005_rh",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local warning = self:GetData("health-warning")
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local flight = self:GetData("flight")
		local vortex = self:GetData("vortex")
		local mat = self:GetData("mat")
		local demat = self:GetData("demat")
		local full = self:GetData("demat-fast")
		local demat_seq = self.interior:GetData("demat_seq2")
		local full_seq1 = self.interior:GetData("full_seq1")

		local part = self.interior:GetPart("coralpogrheostat")
		if not part:GetOn() and power and not teleport and not vortex then
			if demat_seq then
				self.interior:SetData("demat_seq3", true)
			end
			self.interior:SetData("rh", true)
		else
			if demat_seq then
				self.interior:SetData("demat_seq3", false)
			end
			self.interior:SetData("rh", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2005_rs",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local demat = self:GetData("demat")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")
		local demat_seq = self.interior:GetData("demat_seq3")
		local gs = self.interior:GetData("gswitches")
		local bs = self.interior:GetData("bswitches")
		local chance = 2

		local part = self.interior:GetPart("coralpogrs")
		if not part:GetOn() and power and not teleport and not vortex and demat_seq then
            part:EmitSound("poogie/2005/phase.wav")
			self.interior:Timer("demat1", 2.5, function()
				self:Demat()
				self.interior:SetData("event_chance", chance)
			end)
			self.interior:SetData("demat_seq1", false)
			self.interior:SetData("demat_seq2", false)
			self.interior:SetData("demat_seq3", false)
		elseif power and not teleport and not vortex and gs and bs then
			self:ToggleFlight()
		elseif power and teleport and demat and full then
			self.interior:EmitSound("poogie/2005/mat_phase.wav")
        else
			self.interior:SetData("demat_seq3", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2005_bs",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		if power then
			local b1 = self.interior:GetPart("coralpogbswitch")
			local b2 = self.interior:GetPart("coralpogbswitch2")

			if not IsValid(b1) or not IsValid(b2) then return end

			if (b1 == part and not b2:GetOn()) or (b2 == part and not b1:GetOn()) then
				return
			end
			if part:GetOn() then
				self.interior:EmitSound("poogie/2008/power_off.wav")
				self.interior:SetData("bswitches", false)
				return
			end

			if not self.interior:GetData("bswitches") then
				self.interior:EmitSound("poogie/2008/power_on.wav")
				self.interior:SetData("bswitches", true)
			end
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_gs",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		if power then
			local b1 = self.interior:GetPart("coralpoggswitch")
			local b2 = self.interior:GetPart("coralpoggswitch2")

			if not IsValid(b1) or not IsValid(b2) then return end

			if (b1 == part and not b2:GetOn()) or (b2 == part and not b1:GetOn()) then
				return
			end
			if part:GetOn() then
				self.interior:SetData("gswitches", false)
				return
			end

			if not self.interior:GetData("gswitches") then
				self.interior:SetData("gswitches", true)
			end
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_vt",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local flight = self:GetData("flight")
		local vortex = self:GetData("vortex")
		local gs = self.interior:GetData("gswitches")

		if power and not teleport and not vortex and gs then
            self:FastReturn()
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_handbrake",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")

		local part = self.interior:GetPart("coralpoghandbrake")
		if not part:GetOn() and power and not teleport and not vortex then
            self.interior:SetData("brake", true)
        elseif self.interior:GetData("heart_opened") then
			self.interior:SetData("heart_opened", false)
			self.interior:EmitSound("poogie/2005/heart_close.wav")
			self.interior:Timer("heart_closing1", 1, function()
				self.interior:ApplyLightState("heart_closed")
			end)
		else
			self.interior:SetData("brake", false)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_switch",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")

		local part = self.interior:GetPart("coralpogswitch")
		if part:GetOn() and power and not teleport and not vortex then
            self.interior:SetData("poweroff_seq1", true)
		elseif part:GetOn() then
			self.interior:SetData("poweron_seq1", true)
        else
			self.interior:SetData("poweroff_seq1", false)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_lever",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local power_seq1 = self.interior:GetData("poweroff_seq1")
		local event_seq1 = self.interior:GetData("event_seq1")
		local event = self.interior:GetData("flight_event")

		local part = self.interior:GetPart("coralpoglever")
		if not part:GetOn() and power and not teleport and not vortex and power_seq1 then
            self.interior:SetData("poweroff_seq2", true)
        elseif vortex and event then
			self.interior:SetData("event_seq1", true)
		else
			self.interior:SetData("poweroff_seq2", false)
			self.interior:SetData("event_seq2", false)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_bell",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local power_seq2 = self.interior:GetData("poweroff_seq2")

		local part = self.interior:GetPart("coralpogbell")
		if not part:GetOn() and power and not teleport and not vortex and power_seq2 then
            self:TogglePower()
        else
			self.interior:SetData("full_seq1", false)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_stabilisers",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local power_seq2 = self.interior:GetData("poweroff_seq2")

		local part = self.interior:GetPart("coralpogtoggles")
		if not part:GetOn() and power then
            self.interior:SetData("stabilisers", true)
			self:Timer("stabilised", 0.5, function ()
				self.interior:EmitSound("poogie/2005/stabilised.wav")
			end)
        else
			self.interior:SetData("stabilisers", false)
			self:Timer("unstabilised", 0.5, function ()
				self.interior:EmitSound("poogie/2005/unstabilised.wav")
			end)
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_hammer",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local power_seq2 = self.interior:GetData("poweroff_seq2")
		local flight_event = self.interior:GetData("flight_event")
		local event_seq2 = self.interior:GetData("event_seq2")

		local part = self.interior:GetPart("coral_hammer_hitbox")
		if vortex and flight_event and event_seq2 then
            self.interior:SetData("flight_event", false)
			self.interior:SetData("event_chance", 0)
			part:EmitSound("poogie/2005/hammer.wav")
			self.interior:EmitSound("poogie/2005/demat.wav")
		end
	end,
})

TARDIS:AddControl({
	id = "coral2005_lever3",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")
		local rh = self.interior:GetData("rh")
		local brake =  self.interior:GetData("brake")
		local gs = self.interior:GetData("gswitches")
		local bs = self.interior:GetData("bswitches")
		local stabilisers = self.interior:GetData("stabilisers")
		local sw = self.interior:GetData("sw")
		local chance = 5

		local part = self.interior:GetPart("coralpoglever3")
		if not full and power and not teleport and not vortex and gs and bs and rh and stabilisers and sw then
			self.interior:EmitSound("poogie/2005/drum_start.wav")
			self:Timer("lever3_demat", 2, function()
				self:Demat()
				self.interior:SetData("event_chance", chance)
			end)
		else
			self:Mat()
        end
	end,
})

TARDIS:AddControl({
	id = "coral2005_sw_panel",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")

		local part = self.interior:GetPart("coralpoglever3")
		if power and not teleport and not vortex then
			self.interior:SetData("sw", true)
		else
			self.interior:SetData("sw", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2005_lever2",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")

		local part = self.interior:GetPart("coralpoglever2")
		if power and not teleport and not vortex then
			TARDIS:Control("vortex_flight", ply, part)
			if not full then
				self.interior:EmitSound("poogie/2005/full_phase.wav")
			end
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_throttle",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local mat = self:GetData("mat")
		local vortex = self:GetData("vortex")
		local full = self:GetData("demat-fast")
		local rh = self.interior:GetData("rh")
		local brake =  self.interior:GetData("brake")
		local gs = self.interior:GetData("gswitches")
		local bs = self.interior:GetData("bswitches")
		local flight =  self.interior:GetData("flightlever")
		local lever1 = self.interior:GetData("lever1")
		local lever3 = self.interior:GetData("lever3")
		local stabilisers = self.interior:GetData("stabilisers")
		local sw = self.interior:GetData("sw")

		local part = self.interior:GetPart("coralpogthrottle")
		if not part:GetOn() and power and not teleport and not vortex then
			if full and rh and sw and brake and not teleport then
				self:Demat()
			elseif rh and sw and lever3 and gs and bs and not teleport then
				self:Demat()
				self.interior:EmitSound("poogie/2008/speedup.wav")
			end
		elseif part:GetOn() and not teleport and vortex then
			self:Mat()
			self.interior:EmitSound("poogie/2008/speedup.wav")
		elseif part:GetOn() and mat then
			self.interior:EmitSound("poogie/2008/mat_stop.wav")
			util.ScreenShake(self.interior:GetPos(),1,100,1.5,500)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_sw_panel",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")

		local part = self.interior:GetPart("coralpogkeyboard2")
		if not part:GetOn() and power and not teleport and not vortex then
			self.interior:SetData("sw", true)
		else
			self.interior:SetData("sw", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_rh",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")

		local part = self.interior:GetPart("coralpogrheostat")
		if not part:GetOn() and power and not teleport and not vortex then
			self.interior:SetData("rh", true)
		else
			self.interior:SetData("rh", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_handbrake",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local flight = self:GetData("flightlever")

		local part = self.interior:GetPart("coralpoghandbrake")
		if not part:GetOn() and power and not teleport and not vortex then
			self.interior:SetData("brake", true)
		elseif part:GetOn() and power and not teleport and vortex and not flight then
			self.interior:EmitSound("poogie/2008/artron_inc.wav")
			util.ScreenShake(self.interior:GetPos(),7,100,5,700)
		else
			self.interior:SetData("brake", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_hammer",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")

		if power then
			self.interior:EmitSound("poogie/2008/artron_accel.wav")
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_lever1",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")

		local part = self.interior:GetPart("coralpoglever")
		if not part:GetOn() and power and not teleport and not vortex then
			self.interior:SetData("lever1", true)
		else
			self.interior:SetData("lever1", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_lever3",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")

		local part = self.interior:GetPart("coralpoglever3")
		if power and not teleport and not vortex then
			self.interior:SetData("lever3", true)
		elseif power and not teleport and vortex then
			self:Mat()
		else
			self.interior:SetData("lever3", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_flight",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local flight = self:GetData("flight")

		local part = self.interior:GetPart("coralpogflight")
		if part:GetOn() and power and not teleport and not vortex and not flight then
			self.interior:SetData("flightlever", true)
		elseif part:GetOn() and power and not teleport and not vortex then
			self.interior:EmitSound("poogie/2008/speedup.wav")
		else
			self.interior:SetData("flightlever", false)
        end
	end,
})

TARDIS:AddControl({
	id = "coral2008_switch",
	tip_text = nil,
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	ext_func=function(self,ply,part)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport")
		local vortex = self:GetData("vortex")
		local flight = self:GetData("flight")

		if not teleport and vortex then
			self.interior:EmitSound("poogie/2008/drum_vortex.wav")
			TARDIS:Control("fastreturn", ply, part)
        else
			TARDIS:Control("power", ply, part)
		end
	end,
})






