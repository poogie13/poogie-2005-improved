local PART={}
PART.ID = "coralpogflight"
PART.Name = "2005 TARDIS Flight Lever"
PART.Model = "models/doctorwho1200/coral/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.Sound = "poogie/2005/flightlever.wav"


function PART:Initialize()
	self:SetOn(true)
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.flightlever_sound_on or self.Sound
	self.SoundOff = customdata.flightlever_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)