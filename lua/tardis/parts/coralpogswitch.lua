local PART={}
PART.ID = "coralpogswitch"
PART.Name = "2005 TARDIS Switch"
PART.Model = "models/doctorwho1200/coral/switch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.SoundOn = "poogie/2005/switch_on.wav"
PART.SoundOff = "poogie/2005/switch_off1.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.switch_sound_on or self.Sound
	self.SoundOff = customdata.switch_sound_off or self.SoundOn
end


TARDIS:AddPart(PART)