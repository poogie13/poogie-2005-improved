local PART={}
PART.ID = "coralpogvt"
PART.Name = "2005 TARDIS Vector Tracker"
PART.Model = "models/doctorwho1200/coral/vectortracker.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

PART.SoundOn = "poogie/vectortracker.wav"
PART.SoundOff =  "poogie/vt_off.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.AnimateSpeed = customdata.vt_speed or self.AnimateSpeed
	self.SoundOn = customdata.vt_sound_on or self.Sound
	self.SoundOff = customdata.vt_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)