local PART={}
PART.ID = "coralpogrs"
PART.Name = "2005 TARDIS Rotary Switch"
PART.Model = "models/doctorwho1200/coral/rotaryswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3

PART.SoundOn =  "poogie/2005/rs_on.wav"
PART.SoundOff =  "poogie/2005/rs_off.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.rs_sound_on or self.Sound
	self.SoundOff = customdata.rs_sound_off or self.SoundOn
end


TARDIS:AddPart(PART)