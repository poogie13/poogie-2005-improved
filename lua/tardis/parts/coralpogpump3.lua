local PART={}
PART.ID = "coralpogpump3"
PART.Name = "2005 TARDIS Pump 3"
PART.Model = "models/doctorwho1200/coral/pump3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.4

PART.Sound =  "poogie/2008/pump.wav"


TARDIS:AddPart(PART)