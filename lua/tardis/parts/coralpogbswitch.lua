local PART={}
PART.ID = "coralpogbswitch"
PART.Name = "2005 TARDIS Blue Switch"
PART.Model = "models/doctorwho1200/coral/blueswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 7
PART.Sound = "doctorwho1200/coral/plasticswitch.wav"


TARDIS:AddPart(PART)