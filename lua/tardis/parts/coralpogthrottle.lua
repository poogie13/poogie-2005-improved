local PART={}
PART.ID = "coralpogthrottle"
PART.Name = "2005 TARDIS Throttle"
PART.Model = "models/doctorwho1200/coral/throttle.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.SoundOn = "poogie/2005/throttle.wav"
PART.SoundOff = "poogie/2005/throttle_2.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.throttle_sound_on or self.Sound
	self.SoundOff = customdata.throttle_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)