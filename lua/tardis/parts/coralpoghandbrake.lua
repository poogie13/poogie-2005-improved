local PART={}
PART.ID = "coralpoghandbrake"
PART.Name = "2005 TARDIS Handbrake"
PART.Model = "models/doctorwho1200/coral/handbrake.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.SoundOff = "poogie/2005/brake_on.wav"
PART.SoundOn = "poogie/brake_off.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.handbrake_sound_on or self.Sound
	self.SoundOff = customdata.handbrake_sound_off or self.SoundOn
end


TARDIS:AddPart(PART)