local PART={}
PART.ID = "coralpoglever"
PART.Name = "2005 TARDIS Lever"
PART.Model = "models/doctorwho1200/coral/lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.SoundOff = "poogie/2005/flightlever.wav"
PART.SoundOn = "poogie/2005/lever_off.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.lever1_sound_on or self.Sound
	self.SoundOff = customdata.lever1_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)