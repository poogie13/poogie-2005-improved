local PART={}
PART.ID = "coralpoglever3"
PART.Name = "2005 TARDIS Lever 3"
PART.Model = "models/doctorwho1200/coral/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 3
PART.SoundOff = "poogie/brake_off.wav"
PART.SoundOn = "poogie/brake_on.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.lever3_sound_on or self.Sound
	self.SoundOff = customdata.lever3_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)