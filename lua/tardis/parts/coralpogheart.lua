local PART={}
PART.ID = "coral_heart_hitbox"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = false
PART.Name = "Heart"
PART.Model = "models/hunter/plates/plate.mdl"

function PART:Initialize()
	self:SetColor(Color(255,255,255,0))
	self:SetCollisionGroup(COLLISION_GROUP_WORLD)
end


TARDIS:AddPart(PART)