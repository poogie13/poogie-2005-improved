local PART={}
PART.ID = "coralpogrheostat"
PART.Name = "2005 TARDIS Rheostat"
PART.Model = "models/doctorwho1200/coral/rheostat.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.34

PART.Sound =  "poogie/2005/rheostat_on.wav" 


function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.rheostat_sound_on or self.Sound
	self.SoundOff = customdata.rheostat_sound_off or self.SoundOn
    self.AnimateSpeed = customdata.rheostat_speed or self.AnimateSpeed
end

TARDIS:AddPart(PART)