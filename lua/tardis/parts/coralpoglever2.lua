local PART={}
PART.ID = "coralpoglever2"
PART.Name = "2005 TARDIS Lever 2"
PART.Model = "models/doctorwho1200/coral/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5
PART.SoundOn = "poogie/2005/lever2.wav"
PART.SoundOff = "doctorwho1200/coral/throttle.wav"

function PART:Initialize()
	local customdata = self.interior.metadata.CoralControls
	if not customdata then return end

	self.SoundOn = customdata.lever2_sound_on or self.Sound
	self.SoundOff = customdata.lever2_sound_off or self.SoundOn
end

TARDIS:AddPart(PART)