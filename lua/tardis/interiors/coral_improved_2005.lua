-- 2005 TARDIS

local T={

	Base = "tardis2005",
	Name="2005 Improved",
	ID="coral2005_improved",
	IsVesionOf = "tardis2005",
}

T.Interior={
	DoorAnimationTime =15,
	ScreensEnabled = false,
	Sounds={
		Teleport={
			demat="poogie/2005/demat.wav",
			demat_damaged = "poogie/2005/demat_low.wav",
			mat="poogie/2005/mat.wav",
			fullflight = "poogie/2005/fullflight.wav",
			demat_fail = "toast/coral/smeef.wav"
		},
		Door={
			enabled=true,
			open="poogie/2005/door_open_int.wav",
			close="poogie/2005/door_close_int.wav"
		},
		Power = {
			On = "poogie/2005/powerup.wav",
			Off = "poogie/2005/powerdown.wav",
		},
		FlightLoop="poogie/2005/flightloop.wav",
	},

	LightOverride = {
		basebrightness = 0.1,
	},



	Lamps = {
		failed_demat1 = {
				color = Color(180, 0, 0),
				texture = "effects/flashlight/soft",
				fov = 170,
				enabled = false,
				brightness = 3,
				distance = 524,
				brightness = 2.2000000476837,
				pos = Vector(2.375, -40.7802734375, 92.174072265625),
				ang = Angle(28.748170852661, 81.245193481445, 130.67959594727),

				states = {
					demat_f_off = {
						enabled = false,
					},
					demat_f = {
						enabled = true,
					},
				},
		},
		failed_demat2 = {
			color = Color(180, 0, 0),
			texture = "effects/flashlight/soft",
			fov = 170,
			enabled = false,
			brightness = 3,
			distance = 524,
			brightness = 2.2000000476837,
			pos = Vector(-42.65234375, -7.7099609375, 93.604736328125),
			ang = Angle(28.748174667358, 10.988852500916, 130.67959594727),


			states = {
				demat_f_off = {
					enabled = false,
				},
				demat_f = {
					enabled = true,
				},
			},
		},
		failed_demat3 = {
			color = Color(180, 0, 0),
			texture = "effects/flashlight/soft",
			fov = 170,
			enabled = false,
			brightness = 3,
			distance = 524,
			brightness = 2.2000000476837,
			pos = Vector(-17.55029296875, 38.4775390625, 93.627685546875),
			ang = Angle(28.748178482056, -68.357429504395, 130.67959594727),


			states = {
				demat_f_off = {
					enabled = false,
				},
				demat_f = {
					enabled = true,
				},
			},
		},
		failed_demat4 = {
			color = Color(180, 0, 0),
			texture = "effects/flashlight/soft",
			fov = 170,
			enabled = false,
			brightness = 3,
			distance = 524,
			brightness = 2.2000000476837,
			pos = Vector(40.16455078125, 4.3671875, 94.30322265625),
			ang = Angle(28.748174667358, -167.03158569336, 130.67959594727),


			states = {
				demat_f_off = {
					enabled = false,
				},
				demat_f = {
					enabled = true,
				},
			},
		},

		heart_of_tardis = {
			color = Color(197, 255, 186),
			texture = "effects/flashlight/soft",
			fov = 150,
			distance = 554,
			enabled = false,
			nopower = true,
			brightness = 50,
			pos = Vector(19.320495605469, -13.545288085938, 93.02001953125),
			ang = Angle(72.936347961426, -16.603311538696, -25.126708984375),

			states = {
				heart_opened = {
					enabled = true,
				},
				heart_closed = {
					enabled = false,
				},
			},
		},
		heart_of_tardis_2 = {
			color = Color(197, 255, 186),
			texture = "effects/flashlight/soft",
			fov = 150,
			distance = 554,
			enabled = false,
			nopower = true,
			brightness = 50,
			pos = Vector(35.2666015625, -17.814453125, 95.07861328125),
			ang = Angle(83.869689941406, 161.8737487793, 97.434783935547),

			states = {
				heart_opened = {
					enabled = true,
				},
				heart_closed = {
					enabled = false,
				},
			},
		},
		heart_of_tardis_3 = {
			color = Color(197, 255, 186),
			texture = "effects/flashlight/soft",
			fov = 150,
			distance = 554,
			enabled = false,
			brightness = 50,
			nopower = true,
			pos = Vector(26.681610107422, -16.935241699219, 87.6416015625),
			ang = Angle(32.581596374512, -30.20650100708, -124.56216430664),

			states = {
				heart_opened = {
					enabled = true,
				},
				heart_closed = {
					enabled = false,
				},
			},
		},
	},

	Seats={
		{
			pos=Vector(-110,0,60),
			ang=Angle(0,-90,0)
		},
		{
			pos=Vector(-110,20,60),
			ang=Angle(0,-90,0)
		},
		{
			pos=Vector(-110,-20,60),
			ang=Angle(0,-90,0)
		},
	},


	Parts={
		tardis2005_ball1 = false,
		tardis2005_ball2 = false,
		tardis2005_bell = false,
		tardis2005_blueswitch1 = false,
		tardis2005_blueswitch2 = false,
		tardis2005_button = false,
		tardis2005_crank2 = false,
		tardis2005_interior = false,
		tardis2005_greenswitch1 = false,
		tardis2005_greenswitch2 = false,
		tardis2005_handbrake = false,
		tardis2005_handle1 = false,
		tardis2005_handle2 = false,
		tardis2005_lever1b = false,
		tardis2005_pump1 = false,
		tardis2005_pump2 = false,
		tardis2005_pump3 = false,
		tardis2005_pump4 = false,
		tardis2005_regulators1 = false,
		tardis2005_regulators2 = false,
		tardis2005_rheostat = false,
		tardis2005_rotaryswitch = false,
		tardis2005_smallwheel1 = false,
		tardis2005_smallwheel2 = false,
		tardis2005_switch1 = false,
		tardis2005_switch2 = false,
		tardis2005_toggles1 = false,
		tardis2005_toggles2 = false,
		tardis2005_valve1 = false,
		tardis2005_valve2 = false,
		tardis2005_valve3 = false,
		tardis2005_valve4 = false,
		tardis2005_valve5 = false,
		tardis2005_vectortracker = false,
		tardis2005_wheel1 = false,
		tardis2005_wheel2 = false,
		tardis2005_crank1 = false,
		tardis2005_keyboard = false,
		tardis2005_keypad = false,
		tardis2005_lever1a = false,
		tardis2005_lever2 = false,
		tardis2005_lever3 = false,
		tardis2005_radio = false,
		tardis2005_sextant = false,
		tardis2005_monitor =false,
		tardis2005_throttle = false,



		coralconsole=false,

		coralaudio=false,
		coralball=false,
		coralball2=false,
		coralbell=false,
		coralbswitch=false,
		coralbswitch2=false,
		coralbutton=false,
		coralchair=false,

		coralcpipes=false,

		coralcrank=false,
		coralcrank2=false,
		coraldoorframe=false,
		coralflight=false,
		coralgswitch=false,
		coralgswitch2=false,
		coralhandbrake=false,
		coralhandle=false,
		coralhandle2=false,

		coralinterior2=false,
		coralinterior=false,


		tardis2005_seat = false,

		coralkeyboard=false,
		coralkeyboard2=false,
		corallever=false,
		corallever2=false,
		corallever3=false,
		corallights=false,
		coralmonitor=false,
		coralphone=false,

		coralpipes=false,

		coralpump=false,
		coralpump2=false,
		coralpump3=false,
		coralpump4=false,
		coralreg=false,
		coralreg2=false,
		coralrheostat=false,
		coralrs=false,
		coralsextant=false,
		coralstuff=false,
		coralstuff2=false,
		coralstuff3=false,
		coralstuff4=false,
		coralstuff5=false,
		coralstuff6=false,
		coralsw=false,
		coralsw2=false,
		coralswitch=false,
		coralswitch2=false,
		coralthrottle=false,
		coraltoggles=false,
		coraltoggles2=false,
		coralvalve=false,
		coralvalve2=false,
		coralvalve3=false,
		coralvalve4=false,
		coralvalve5=false,
		coralvt=false,

		coralwalls=false,

		coralwheel=false,
		coralwheel2=false,
		coralscreen=false,




		coralpogaudio=true,
		coralpogconsole=false,
		coralpogball=true,
		coralpogball2=true,
		coralpogbell=true,
		coralpogbswitch=true,
		coralpogbswitch2=true,
		coralpogbutton=true,
		coralpogchair=true,
		coralpogcpipes=false,
		coralpogcrank=true,
		coralpogcrank2=true,
		coralpogdoorframe=false,
		coralpogflight=true,
		coralpoggswitch=true,
		coralpoggswitch2=true,
		coralpoghandbrake=true,
		coralpoghandle=true,
		coralpoghandle2=true,
		coralpoginterior2=false,
		coralpogkeyboard=true,
		coralpogkeyboard2=true,
		coralpoglever={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		coralpoglever2=true,
		coralpoglever3=true,
		coralpoglights=false,
		coralpogmonitor=true,
		coralpogphone=true,
		coralpogpipes=false,
		coralpogpump=true,
		coralpogpump2=true,
		coralpogpump3=true,
		coralpogpump4=true,
		coralpogreg=true,
		coralpogreg2=true,
		coralpogrheostat=true,
		coralpogrs=true,
		coralpogsextant=true,
		coralpogstuff=false,
		coralpogstuff2=false,
		coralpogstuff3=false,
		coralpogstuff4=false,
		coralpogstuff5=false,
		coralpogstuff6=false,
		coralpogsw=true,
		coralpogsw2={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		coralpogswitch2=true,
		coralpogswitch=true,
		coralpogthrottle=true,
		coralpogthrottle=true,
		coralpogtoggles=true,
		coralpogtoggles2=true,
		coralpogvalve=true,
		coralpogvalve2=true,
		coralpogvalve3=true,
		coralpogvalve4=true,
		coralpogvalve5=true,
		coralpogvt=true,
		coralpogwalls=false,
		coralpogwheel=true,
		coralpogwheel2={
			pos=Vector(0,0,0),
			ang=Angle(0,180,0),
		},
		coral_hammer_hitbox={
			pos = Vector(-19.806, 46.669, 44.695),
			ang = Angle(48.113, 31.254, 11.622),
		},
		coral_heart_hitbox={
			pos = Vector(28.079, -16.397, 72.558),
			ang=Angle(0,180,0),
		}
	},
    Scanners = {
        {
			part = "coralpogmonitor",
            mat = "models/doctorwho1200/coral/screen",
            width = 1024,
            height = 1024,
            ang = Angle(0,90,0),
            fov = 90,
        }
    },

	Controls = {
		coralpogredbutton = "fastreturn",
		coralpogaudio = "music",
		coralpogball2 = "thirdperson",
		coralpogkeypad = false,
		coralpogconsole = false,
		coralpogcrank1 = false,
		coralpogswitch1 = false,
		coralpoglever1a = false,
		coralpogswitch2 = "power",
		coralpoglever3 = false,
		coralpoglever2 = "coral2005_lever2",
		coralpogreg = "random_destination",
		coralpogradio = "music",
		coralpogsextant = false,
		coralpogsw = "flight",
		coralpogsw2 = "float",
		coralpogball = "destination",
		coralpogbutton = "doorlock",
		coralpogthrottle = "coral2005_throttle",
		coralpogball1 = false,
		coralpogball2 = false,
		coralpoghandbrake = "false",
		coralpogvalve5 = "toggle_screens",
		coralpogtoggles1 = false,
		coralpogtoggles = "coral2005_stabilisers",
		coralpogcrank2 = "handbrake",
		coralpogrheostat = "coral2005_rh",
		coralpogtoggles2 = false,
		coralpogkeyboard = "coordinates",
		coralpoghandle2 = "toggle_scanners",
		coralpoggreenswitch1 = false,
		coralpogflight = "coral2005_flightlever",
		coralpoghandbrake = "coral2005_handbrake",

		coralpoggreenswitch2 = false,

		coralpogblueswitch2 = "shields",
		coralpogrs = "coral2005_rs",

		coralpogregulators2 = false,
		coralpogpump1 = "engine_release",

		coralpogbswitch = "coral2005_bs",
		coralpogbswitch2 = "coral2005_bs",
		coralpoggswitch = "coral2005_gs",
		coralpoggswitch2 = "coral2005_gs",
		coralpogvt = "coral2005_vt",
		coralpogbell = "coral2005_bell",
		coralpoglever = "coral2005_lever",
		coralpogswitch = "coral2005_switch",
		coral_hammer_hitbox = "coral2005_hammer",
		coralpogkeyboard2 = "coral2005_sw_panel",
		coralpoglever3 = "coral2005_lever3",
		coralpoghandle = "isomorphic",

		tardis2005_console = "thirdperson_careful",

		coralconsole = false,
		coralbswitch2 = false,
		coralgswitch = false,
		coralkeyboard2 = false,
		corallever = false,
		corallever2 = false,
		corallever3 = false,
		coralsextant = false,
		coraltoggles = false,
		coralswitch = false,
		coralbutton = false,
		coralthrottle = false,
		coralhandbrake = false,
		coralflight = false,
		coralvalve5 = false,
		coralkeyboard = false,
		coralphone = false,
	},

}
T.Exterior={
	DoorAnimationTime = 0.65,
	ProjectedLight = {
        --color = Color(r,g,b), --Base color. Will use main interior light if not set.
        --warncolor = Color(r,g,b), --Warning color. Will use main interior warn color if not set.
        brightness = 0.001, --Light's brightness
        --vertfov = 90,
        --horizfov = 90, --vertical and horizontal field of view of the light. Will default to portal height and width.
        farz = 50, --FarZ property of the light. Determines how far the light projects.]]
        offset = Vector(-21,0,51.1), --Offset from box origin
    },
	Sounds={
		Teleport={
			demat="poogie/2005/demat_ext.wav",
			demat_damaged = "poogie/2005/demat_low.wav",
			mat="poogie/2005/mat.wav",
			fullflight = "poogie/2005/full.wav",
		},
		Door={
			enabled=true,
			open="poogie/2008/door_open_ext.wav",
			close="poogie/2005/door_close_ext1.wav"
		},
		FlightLoop="poogie/2005/flight_loop_ext.wav",
	},
    Chameleon = {
        AnimTime = 1,
    },
    Teleport = {
        SequenceSpeedFast = 0.9,
        SequenceSpeed = 0.7,
		SequenceSpeedWarning = 0.83,
        DematSequence = {
            200,
            100,
            150,
            50,
            100,
            0
        },
        MatSequence = {
            50,
            150,
            100,
            200,
            150,
            255
        }
    }
}

T.Templates = {
	tardisman_colors_2005 = {override = true},
	tardisman_coral_texture_sets_interior = {override = true},
	tardisman_ab_box = {override = true},
	tardisman_coral_texture_sets_exterior_ab = {override = true,},
	coral_improved_2005_sounds = {override = true,},
}

T.CustomHooks = {

	demat_shake_or_whatever = false,
	handbrake_initial_value_i_think = false,

	fail_demat = {
		exthooks = {["HandleNoDemat"] = true,},
		inthooks = {},
		func = function(ext, int)
			if not IsValid(int) then return end
			if SERVER and not ext:GetData("vortex") and not ext:GetData("teleport") then
				int:ApplyLightState("demat_f")
				int:Timer("demat_fail_stop", 4, function()
					int:ApplyLightState("demat_f_off")
				end)
			end
		end,
	},
	shakes_full = {
		exthooks = {["DematStart"] = true,},
		inthooks = {},
		func = function(ext, int)
			if not IsValid(int) or not ext:GetData("demat-fast") or int:GetData("stabilisers") then return end
			util.ScreenShake(int:GetPos(),2,100,20,300)
		end,
	},
	flight_event = {
		exthooks = {["StopDemat"] = true,},
		inthooks = {},
		func = function(ext, int)
			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning")
			local teleport = ext:GetData("teleport")
			local flight = ext:GetData("flight")
			local vortex = ext:GetData("vortex")
			local mat = ext:GetData("mat")
			local demat = ext:GetData("demat")
			local full = ext:GetData("demat-fast")
			local chance = int:GetData("event_chance")
			local stabilisers = int:GetData("stabilisers")
			if CLIENT then return end

			if not full and vortex and math.random(1, (chance or -1)) == 1 then
				ext:Timer("event1", 5, function()
					if vortex then
						util.ScreenShake(int:GetPos(),5,100,2,700)
						int:EmitSound("poogie/2005/flight_event.wav")
						int:SetData("flight_event", true)
					end
				end)
				ext:Timer("event2", 15, function()
					if int:GetData("flight_event") then
						util.ScreenShake(int:GetPos(),5,100,5,700)
						int:EmitSound("poogie/2005/flight_fail.wav")
						int:EmitSound("poogie/2008/power_off.wav")
						ext:ToggleWarning()
						ext:SetRandomDestination(grounded)
						ext:Mat()
					end
				end)
			end
			stability = 0
		end,
	},
	flight_event_mat = {
		exthooks = {["StopMat"] = true,},
		inthooks = {},
		func = function(ext, int)
			if CLIENT then return end

			if int:GetData("flight_event") then
				ext:ToggleWarning()
				int:SetData("flight_event", false)
				int:SetData("event_chance", 0)
			end
		end,
	},
	heart_opening = {
		exthooks = {["PowerToggled"] = true,},
		inthooks = {},
		func = function(ext, int)
			if not IsValid(int) then return end
			if CLIENT then return end

			if int:GetData("health-warning") and int:GetPower() == false and math.random(1, 1) == 1 then
				int:Timer("heart", 5, function()
					if int:GetPower() == false then
						int:SetData("heart_opened", true)
						int:EmitSound("poogie/2005/heart_open.wav")
						int:Timer("opening", 2.6, function()
							int:ApplyLightState("heart_opened")
						end)
					end
				end)
			elseif int:GetPower() == true and int:GetData("heart_opened") == true then
				int:SetData("heart_opened", false)
				int:EmitSound("poogie/2005/heart_close.wav")
				int:Timer("closing1", 1, function()
					if int:GetPower() == true then
						int:ApplyLightState("heart_closed")
					end
				end)
			end
		end,
	},
}

TARDIS:AddInterior(T)

TARDIS:AddCustomVersion("tardis2005", "coral2005_improved", {
	id = "coral2005_improved",
	name = "2005 Improved",
})